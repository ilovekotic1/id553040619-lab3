package yardsuntea.thanaphat.lab3;

public class Permutation {

	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 2){
			System.err.println("Usage:Permutation <n> <k> ");
			System.exit(1);
		}
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		long result = permute(n , k);
		System.out.println(n + "!/(" + n + "-" + k + ")! = " + result);

	}

	private static long permute(int n, int k) {
		int x = n;
		int y = n - k;
		int a = 1;
		int b = 1;

		if (n == 0 || k == 0 || k > n) {
			return 0;
		}

		while (x > 1) {							
			a = a * x;							
			x--;								
		}										
		System.out.println(n + "!= " + a); 		

		while (y > 1) {
			b = b * y;
			y--;
		}
		System.out.println("(" + n + " - " + k + ")! = " + b);

		int j = a / b;

		return j;
	}

}
