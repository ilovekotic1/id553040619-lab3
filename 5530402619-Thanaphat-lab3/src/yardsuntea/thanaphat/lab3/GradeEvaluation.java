package yardsuntea.thanaphat.lab3;

public class GradeEvaluation {

	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 1){
			System.err.println("Usage:GradeEvaluation <scroe> ");
			System.exit(0);
		}
		double score = Double.parseDouble(args[0]);
		computeGrade(score);
	}

	private static void computeGrade(double score) {
		if (score >= 80 && score <= 100) {
			System.out.println("The for score " + score + " is A");
		} else if (score >= 70 && score <= 79) {
			System.out.println("The for score " + score + " is B");
		} else if (score >= 60 && score <= 69) {
			System.out.println("The for score " + score + " is C");
		} else if (score >= 50 && score <= 59) {
			System.out.println("The for score " + score + " is D");
		} else if (score > 100 || score < 0) {
			System.out.println(score + " is an invalid score. Please enter score in the range 0 - 120");
		} else {
			System.out.println("The grade for score " + score + " is F");
		}
	}

}
